﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace PAIN.WPFW10.Kalkulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private String operand1 = "2";
        private String operand2 = "4";
        private String mathOperator = "*";
        private double result = 8;
        private bool hasOperand1Focus = false;
        private bool hasOperand2Focus = false;

        public String Operand1
        {
            get
            {
                return operand1;
            }
            set
            {
                operand1 = value;
            }
        }
        
        public String Operand2
        {
            get
            {
                return operand2;
            }
            set
            {
                operand2 = value;
            }
        }

        public String MathOperator
        {
            get
            {
                return mathOperator;
            }
            set
            {
                mathOperator = value;
            }
        }

        public double Result
        {
            get
            {
                return result;
            }
            set
            {
                result = value;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void calculateResult()
        {
            double numberOperand1 = Double.Parse(this.operand1);
            double numberOperand2 = Double.Parse(this.operand2);
            switch(this.mathOperator)
            {
                case "+":
                    this.result = numberOperand1 + numberOperand2;
                    break;
                case "-":
                    this.result = numberOperand1 - numberOperand2;
                    break;
                case "*":
                    this.result = numberOperand1 * numberOperand2;
                    break;
                case "/":
                    this.result = numberOperand1 / numberOperand2;
                    break;
            }
        }

        private void number0Button_Click(object sender, RoutedEventArgs e)
        {
            if(this.hasOperand1Focus)
            {
                this.Operand1 += "0";
                OnPropertyChanged("Operand1");
            }
            else if(this.hasOperand2Focus)
            {
                this.Operand2 += "0";
                OnPropertyChanged("Operand2");
            }
        }

        private void number1Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "1";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "1";
                OnPropertyChanged("Operand2");
            }
        }

        private void number2Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "2";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "2";
                OnPropertyChanged("Operand2");
            }
        }

        private void number3Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "3";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "3";
                OnPropertyChanged("Operand2");
            }
        }

        private void number4Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "4";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "4";
                OnPropertyChanged("Operand2");
            }
        }

        private void number5Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "5";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "5";
                OnPropertyChanged("Operand2");
            }
        }

        private void number6Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "6";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "6";
                OnPropertyChanged("Operand2");
            }
        }

        private void number7Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "7";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "7";
                OnPropertyChanged("Operand2");
            }
        }

        private void number8Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "8";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "8";
                OnPropertyChanged("Operand2");
            }
        }

        private void number9Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 += "9";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 += "9";
                OnPropertyChanged("Operand2");
            }
        }

        private void equalButton_Click(object sender, RoutedEventArgs e)
        {
            calculateResult();
            OnPropertyChanged("Result");
        }

        private void plusButton_Click(object sender, RoutedEventArgs e)
        {
            this.MathOperator = "+";
            OnPropertyChanged("MathOperator");
        }

        private void minusButton_Click(object sender, RoutedEventArgs e)
        {
            this.MathOperator = "-";
            OnPropertyChanged("MathOperator");
        }

        private void multiplyButton_Click(object sender, RoutedEventArgs e)
        {
            this.MathOperator = "*";
            OnPropertyChanged("MathOperator");
        }

        private void divideButton_Click(object sender, RoutedEventArgs e)
        {
            this.MathOperator = "/";
            OnPropertyChanged("MathOperator");
        }

        private void backspaceButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                if (operand1.Length > 0)
                {
                    this.Operand1 = this.operand1.Remove(operand1.Length - 1);
                    OnPropertyChanged("Operand1");
                }
            }
            else if (this.hasOperand2Focus)
            {
                if (operand2.Length > 0)
                {
                    this.Operand2 = this.operand2.Remove(operand2.Length - 1);
                    OnPropertyChanged("Operand2");
                }
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            this.Operand1 = "";
            this.Operand2 = "";
            this.MathOperator = "";
            this.Result = 0;
            OnPropertyChanged("Operand1");
            OnPropertyChanged("Operand2");
            OnPropertyChanged("MathOperator");
            OnPropertyChanged("Result");
        }

        private void clearEntryButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                this.Operand1 = "";
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                this.Operand2 = "";
                OnPropertyChanged("Operand2");
            }
        }

        private void negativeButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                if (this.operand1[0].ToString() != "-")
                    this.Operand1 = "-" + Operand1;
                else
                    this.Operand1 = operand1.Remove(0, 1);
                OnPropertyChanged("Operand1");
            }
            else if (this.hasOperand2Focus)
            {
                if (this.operand2[0].ToString() != "-")
                    this.Operand2 = "-" + Operand2;
                else
                    this.Operand2 = operand2.Remove(0, 1);
                OnPropertyChanged("Operand2");
            }
        }

        private void comaButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.hasOperand1Focus)
            {
                if (!this.Operand1.Contains(","))
                {
                    this.Operand1 += ",";
                    OnPropertyChanged("Operand1");
                }
            }
            else if (this.hasOperand2Focus)
            {
                if (!this.Operand2.Contains(","))
                {
                    this.Operand2 += ",";
                    OnPropertyChanged("Operand2");
                };
            }
        }

        private void operand1TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.hasOperand1Focus = true;
            this.hasOperand2Focus = false;
        }

        private void operand2TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.hasOperand1Focus = false;
            this.hasOperand2Focus = true;
        }

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // INotifyPropertyChanged Implementation

        private void operand1TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private void operand2TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9,-]+");
            return !regex.IsMatch(text);
        }
    }
}
